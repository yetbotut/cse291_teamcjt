//
// half stepper motor

#include "mbed.h"

Serial pc(USBTX, USBRX);


DigitalOut blue_pin(p24);
DigitalOut pink_pin(p23);
DigitalOut yellow_pin(p22);
DigitalOut orange_pin(p21);


void rotate(int a, int b, int c, int d) {
    orange_pin = a;
    yellow_pin = b;
    pink_pin   = c;
    blue_pin   = d;
}

int main() {
    double wait_time = 0.001;
    while (1) {
       
        // step 8
        wait(wait_time);
        rotate(0, 0, 0, 1);
        
        // step 7
        wait(wait_time);
        rotate(0, 0, 1, 1);
        
        // step 6
        wait(wait_time);
        rotate(0, 0, 1, 0);
        
        // step 5
        wait(wait_time);
        rotate(0, 1, 1, 0);
        
        // step 4
        wait(wait_time);
        rotate(0, 1, 0, 0);
        
        // step 3
        wait(wait_time);
        rotate(1, 1, 0, 0);
        
        // step 2
        wait(wait_time);
        rotate(1, 0, 0, 0);
        
        // step 1
        wait(wait_time);
        rotate(1, 0, 0, 1);
   }
}